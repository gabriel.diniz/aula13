import java.util.Scanner;

public class aula13 {  //calculadora 2.0
    public static void main(String[] args) {  // metodo estatico principal

        Scanner in = new Scanner(System.in);

        System.out.println("Digite 1 para somar");  // imprime a mensagem
        System.out.println("Digite 2 para subtrair"); // imprime a mensagem
        System.out.println("Digite 3 para multiplicar"); // imprime a mensagem
        System.out.println("Digite 4 para dividir"); // imprime a mensagem

        int i = in.nextInt(); // variavel nome i que pega o valor digitado

        System.out.println("Digite o primeiro número: "); // imprime a mensagem
        double num1 = in.nextDouble(); // variavel tipo double armazena o valor 1

        System.out.println("Digite o segundo número: "); // imprime a mensagem
        double num2 = in.nextDouble(); // variavel tipo double armazena o valor 2

        if (i == 1){  // se i é igual  1 soma
            System.out.println(num1 + num2); // imprimir o valor da soma
        }else{ // se não
            if(i == 2) { // se i é igual  2 subtrai
                System.out.println(num1 - num2); // imprimir o valor da subtração
            }else{  // se não
                if(i == 3) { // se i é igual  3 multiplica
                    System.out.println(num1 * num2); // imprimir o valor da multiplicação
                }else {  //se não
                    if (i == 4) { // se i é igual  4 divide
                        System.out.println(num1 / num2);  // imprimir o valor da divisão
                    }else{ // se não
                        System.out.println("Operação inválida"); // imprime mensagem

                    }
                }
            }
        }
    }
}
